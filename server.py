from http.server import HTTPServer, BaseHTTPRequestHandler
import subprocess
import json
import os
import sys

VERSION = "0.0.1.0" # increment this when we change secc
BASE_URL = "https://covern.org"
TERMS_OF_USE_URL = "https://bitbucket.org/covern/secc/src/master/LICENSE"
PRIVACY_URL = BASE_URL # + ...
INSTITUTION_URL = BASE_URL # + ...
TOOL_URL = BASE_URL + "/secc"
IMAGE_URL = "https://bitbucket.org/covern/secc/raw/master/logo.png"

SYNTAX = r"""
{
  displayName:    'SecC',
  name:           'secc',
  mimeTypes:      ['text/x-secc'],
  fileExtensions: ['.c'],
  
  lineComments:      '//',
  blockCommentStart: '/*',
  blockCommentEnd:   '*/',

  keywords: ['assume', 'assert', 'ensures', 'exists', 'requires', 'int', 'void', 'return', 'if', 'while', 'else', 'fails', 'struct', 'bool', 'typedef', 'enum'],

  operators: ['+','|->'],

  
  tokenizer: {
    root: [
      
      ['[a-z_$][\\w$]*', { cases: { '@keywords': 'keyword',
                                   '@default': 'identifier' } }],
      
      
      { include: '@whitespace' },
      
      
      ['[{}()\\[\\]]', '@brackets'],
   ],

    whitespace: [
      ['[ \\t\\r\\n]+', 'white'],
      ['\\/\\*',       'comment', '@comment' ],
      ['\\/\\/.*$',    'comment'],
    ],

    comment: [
      ['[^\\/*]+', 'comment' ],
      ['\\/\\*',    'comment.invalid' ],
      ["\\*/",    'comment', '@pop'  ],
      ['[\\/*]',   'comment' ]
    ],  
  },
}
"""


metadata = ""
def populate_metadata():
    global metadata
    samples = []
    print (str(examples))
    for (f,src) in examples:
        samples.append({"Name": f, "Source": src})

    metadata = {
        "Name": "secc",
        "DisplayName": "SecC",
        "Version": VERSION,
        "TermsOfUseUrl": TERMS_OF_USE_URL,
        "PrivacyUrl": PRIVACY_URL,
        "Institution": "COVERN Project",
        "InstitutionUrl": INSTITUTION_URL,
        "InstitutionImageUrl": IMAGE_URL,
        "MimeType": "text/x-secc",
        "Title": "SecC",
        "Description": "Verifier for Security Concurrent Separation Logic (SecCSL)",
        "Question": "Is this code secure?",
        "Url": TOOL_URL,
        "SupportsLanguageSyntax": True,
        "Samples": samples
    }
        
    metadata = bytes(json.dumps(metadata, indent=4, sort_keys=True),
                     'utf-8')

        
        
def secc_binary():
    return (os.path.join(secc_path,"secc"))

def secc_examples_dir():
    return (os.path.join(secc_path,"examples"))

examples = []
def populate_examples():
    global examples
    contents = os.listdir(secc_examples_dir())
    for c in contents:
        fullc = os.path.join(secc_examples_dir(),c)
        if os.path.isfile(fullc):
            if c.endswith(".c"):
                print ("adding example %s" % c)
                src = open(fullc,"r")
                examples.append((c,src.read()))
                src.close()
        
                
def check_secc_path():
    if not os.path.isdir(secc_path):
        raise Exception("secc path not a directory")
        

class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):
        
    def do_GET(self):
        path = self.path.split('?')[0]
        if path == "/metadata":
            self.send_response(200)
            self.end_headers()
            self.wfile.write(metadata)
        elif path == "/language":
            self.send_response(200)
            self.end_headers()
            self.wfile.write(bytes(SYNTAX,'utf-8'))
        else:
            print("Don't know: %s" % path)
            self.send_response(404)
            self.end_headers()
            
            
        

    def do_POST(self):
        if self.path == "/run":
            content_length = int(self.headers['Content-Length'])
            body = self.rfile.read(content_length)
            bodystr = body.decode('utf-8')
            print("Here is the body: ")
            print(bodystr)
            try:
                j = json.loads(bodystr)

                source = j["Source"]
                p = subprocess.Popen([secc_binary(),"-"],stdin=subprocess.PIPE,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
                (stdout,stderr) = p.communicate(bytes(source,'utf-8'))
                
                response = {
                    "Version": VERSION,
                    "Outputs": [ {"MimeType": "text/plain", "Value": "stdout: " + stdout.decode('utf-8') + "\nstderr: " + stderr.decode('utf-8') + "\n"  } ]
                }
                s = json.dumps(response, indent=4, sort_keys=True)
                self.send_response(200)
                self.end_headers()
                self.wfile.write(bytes(s,'utf-8'))
            except:
                self.send_response(400)
                self.end_headers()
                self.wfile.write(b'Error')
                raise
                
        else:
            self.send_response(404)
            self.end_headers()


if len(sys.argv) == 1:
    sys.stderr.write("Usage: %s <path-to-secc>\n" % sys.argv[0])
    sys.exit(1)

secc_path = sys.argv[1]
check_secc_path()
populate_examples()
populate_metadata()

httpd = HTTPServer(('', 8000), SimpleHTTPRequestHandler)
httpd.serve_forever()
